const christmas_eve="25 Dec 2022";
const days_ = document.getElementById("days");
const hours_ = document.getElementById("hours");
const mins_ = document.getElementById("minutes");
const seconds_= document.getElementById("seconds")

function countdown(){
     const  christmasevedate=new Date(christmas_eve);
     const currentDate=new Date();
     const totalSeconds=(christmasevedate-currentDate)/1000;
     const days = Math.floor(totalSeconds / 3600 / 24);
     const hours = Math.floor(totalSeconds / 3600) % 24;
     const mins = Math.floor(totalSeconds / 60) % 60;
     const seconds = Math.floor(totalSeconds) % 60;
     days_.innerHTML=days;
     hours_.innerHTML=hours;
     mins_.innerHTML=mins;
     seconds_.innerHTML=seconds;
    }

countdown();
setInterval(countdown,1000);