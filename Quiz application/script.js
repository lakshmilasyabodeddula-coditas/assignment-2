const quizData = [
    {
        question: "Number of colors in a rainbow are?",
        a: "11",
        b: "2",
        c: "1",
        d: "7",
        correct: "d",
    },
    {
        question: "Who is the Prime minister of india?",
        a: "Rahul gandhi",
        b: "Narendra Modi",
        c: "Sonia Gandhi",
        d: "Amit sha",
        correct: "b",
    },
    {
        question: "Who is the ceo of facebooj?",
        a: "MarkZuker berg",
        b: "Sundhar pichai",
        c: "Satya nadella",
        d: "Ali baba",
        correct: "a",
    },
    {
        question: "Year of independence for India is",
        a: "1947",
        b: "1948",
        c: "1949",
        d: "none of the above",
        correct: "a",
    },
];

const quiz = document.getElementById("quiz");
const answerEls = document.querySelectorAll(".answer");
const questionEl = document.getElementById("question");
const a_text = document.getElementById("a_text");
const b_text = document.getElementById("b_text");
const c_text = document.getElementById("c_text");
const d_text = document.getElementById("d_text");
const submitBtn = document.getElementById("submit");

let currentQuiz = 0;
let score = 0;

loadQuiz();

function loadQuiz() {
    deselectAnswers();

    const currentQuizData = quizData[currentQuiz];

    questionEl.innerText = currentQuizData.question;
    a_text.innerText = currentQuizData.a;
    b_text.innerText = currentQuizData.b;
    c_text.innerText = currentQuizData.c;
    d_text.innerText = currentQuizData.d;
}

function getSelected() {
    let answer = undefined;

    answerEls.forEach((answerEl) => {
        if (answerEl.checked) {
            answer = answerEl.id;
        }
    });

    return answer;
}

function deselectAnswers() {
    answerEls.forEach((answerEl) => {
        answerEl.checked = false;
    });
}

submitBtn.addEventListener("click", () => {
    // check to see the answer
    const answer = getSelected();

    if (answer) {
        if (answer === quizData[currentQuiz].correct) {
            score++;
        }

        currentQuiz++;
        if (currentQuiz < quizData.length) {
            loadQuiz();
        } else {
            quiz.innerHTML = `
                <h2>You answered correctly at ${score}/${quizData.length} questions.</h2>
                
                <button onclick="location.reload()">Reload</button>
            `;
        }
    }
});